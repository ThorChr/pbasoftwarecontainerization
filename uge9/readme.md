# Apache stuff
Har lidt forvirring mellem opgave 1 og 2. Da min løsning vilel være noget der passer til begge, it seems? Men oh well.

Løsningen jeg ville gå efter ville være at lave en ```docker run``` af et httd image, hvor jeg mounter en volume der svarer til min hjemmeside.

```bash
docker run -dit --name my-apache-server -p 8080:80 -v /path/to/my-website:/usr/local/apache2/htdocs/ httpd:2.4
```

Den final command jeg endte med at g�re brug af var:

```bash
docker run -dti --name apachev2 -p 8081:80 -v ./www/:/usr/local/apache2/htdocs/ httpd
```

Resultatet var, at det virkede. jeg fik hjemmesiden op.

**WRONG WRONG WRONG WRONG**

Jeg endte med at finde ud af hvad jeg skrev der faktisk *ikke virkede*. Sandt nok fik jeg smidt noget op, men det var ikke hvad jeg ønskede der blev smidt op. 

Problemet var med måden jeg mountede ting på. Det kræver åbenbart den fulde absolute path fra host maskinen. Så jeg lavede ændring til min command, og fik den til sidst til at virke. Jeg gjorde brug af ```PWD```.

```bash
docker run -dti --name apachev3 -p 8083:80 -v $PWD/www/:/usr/local/apache2/htdocs/ httpd
```

Denne virker fra en Windows maskine.

## Notes

Det som g�r den her opgave speciel, er det faktum at man skal finde ud af hvor tingene skal placeres.
Det virker til at Apache ønsker at hjemmeside der skal blive fremvist skal være i en mappe (på containeren) der hedder ```/usr/local/apache2/htdocs/```.

Dog når man ved det, så er det meget nemt. Så handler det bare om at mount ens hostmachine mappe til container mappen, og så køre containeren.

Derudover var der så også den dumme fejl med at det kræver den absolute path fra host maskinen. Men så snart man ved det, så yes.

# Oprydning

Den her opgave handler om at fjerne noget af alt det shit vi tidligere har gjort brug af.

Til at starte med, man kan finde ud af hvor meget plads Docker faktisk gør brug af ved at bruge følgende command:

```bash
docker system df
TYPE            TOTAL     ACTIVE    SIZE      RECLAIMABLE
Images          10        7         1.183GB   394.3MB (33%)
Containers      19        1         256.4MB   256.4MB (99%)
Local Volumes   2         2         195.9MB   0B (0%)
Build Cache     310       0         6.531GB   6.531GB
```

Hvis man vil, kan man også gøre brug af ```-v``` flaget for at gøre svaret mere verbose, og derved få mere info.

For at rydde alt op i docker, kan man gøre brug af:

```docker system prune -a```

```-a``` inkludere ubrugte og 'dangling' containere. Uden det tag, ville det kun være untagget images der blev yeeted.

Hvis man i stedet ønsker at reclaim en del plads, men fadtholde *tagged images*, så kan man bruge denne command:

```bash
docker system prune
WARNING! This will remove:
  - all stopped containers
  - all networks not used by at least one container
  - all dangling images
  - all dangling build cache

Are you sure you want to continue? [y/N] y
Deleted Containers:
10ceaf90bf5795b206ef052e476f2ebf2ca5afd6014cb67de135f3e39cc2372d
602e82103b0d038e16102c53e68b0910604414454d7c8af3f755fe076918aca0
c8bb61c56282eecfb7d962b7775fafbbbedd36a15877d5fab2ba254533ccd7e3
59bfd58b5d686fbee6105bf5637f5dc2daf2416788afb3fef83a5d026f35c38e
fa983c5999ff6f74982b3cdbf9fa0cd73ab455cd3c2afa7082aa65c9c98280d8
8216c1e8ad83bac4f099aab6212049114f6dabe3c9daf25a5126f29f03614370
f3da2654818ad8bffa8c269a903f3b985f4cd6510acc906505a1df834a338bb8
cf8d6b190f7d22e84da35fec1e3581035914849d4990612728383b683a8df05d
7792c074c728a5786c794092e1d3f62034c2a3ba2ee81c6e5b5a3ac1bd7e45aa
0ae43ef928fe2a742bb7919d2cff96ee188c9eedeaa72eddc0290e25c12b0b2d
c5bd0c93e9fd3e1ef2792a5864fe3f40a7e8eed6200735cfb0ca37d0641c6f10
47d2168c63236c8749acc78380ab40eae7f023c4e288aa7d5e641d92516ac02c
c2ab5d13d8bb3367a1cca7df152ac491575d7e00dfadfcac2dd4b9fb4b22081d
28e4b1748fd1321007eb9a4b35c704cc85f494b15514952ff033bd6dff543740
9072cd6fcc6bfccb2da07d6574c236ca802ac77f32a8a9fdd1b71090e112b996
ecbdabfd0f5f82acc9e65539b960ee46ee7fedd769c3835588b6675cb6c6805f
0180ec825b1f7b9ed4ff77e56cff0568876e71a770fa99fe362fccef3f4ec728
2dbc3c5327427c15dbeebc81464272981abdf306781196c73ebd982adcf4a22a

Deleted Networks:
hackathon_default

Deleted build cache objects:
r7jhnhh4np4czrw2aaa7l9la3
kjulcubwiq8jwi7nq9t7iv8gz
luvzpfsrqyp3zkb9c2b605bj5
mg9xudb2qwg54ywewck3ss579
j7aow8yef0bz1x0oa0chpk65t
...
Total reclaimed space: 6.787GB
```

## Opsummering

Slet alt, bortset fra det som er i brug: ```docker system prune -a```
Slet alt, bortset fra det som er i brug og tagget images: ```docker system prune```
Slet ubrugte og dangling images: ```docker image prune```
Slet dangling images only: ```docker image prune -a```
Slet stopped containere: ```docker container prune```
Slet ubrugte volumes: ```docker volume prune```

N�r man sleter stuff kommer der en confirmation ting frem. For at ignore den, kan man bruge flaget ```-f``` for bare at sige "yes".


# Commit af container

Opgaven her handler om at jeg skal lave min egen Nginx server baseret fra en Debian container. Hvorefter jeg så skal lave et commit af den, og derved gå fra container -> image.

Første skridt er at oprette container.
Andet er installation af Nginx.
Tredje er oprettelse af basal hjemmeside.
Fjerde er ```docker commit```.

## Første skridt

```bash
docker run -dti debian
```

Så for at gå ind i den gør jeg:

```bash
docker exec -it 95 bash
```

## Andet skridt

Tl at starte med, lad os opdatere whatever repo der bliver brugt:

```bash
apt update
```

Bagefter kan man installere Nginx.

```bash
apt install nginx
```

## Tredje skridt

Hmm, stackoverflow giver minimum 3 forskellige mulige pladser at hjemmesiden ligger ved... Det er et problem.

```
If installing on Ubuntu using apt-get, try /usr/share/nginx/www.

EDIT:

On more recent versions the path has changed to: /usr/share/nginx/html

2019 EDIT:

Might try in /var/www/html/index.nginx-debian.html too.
```

Jeg tror at det var den sidste der virkede for mig.

## Fjerde skridt

Nu handler det så om at omvandle min debian til et image. Den her er heldigvis meget nem:

```bash
docker commit 83f60ec8e5af my-debian-nginx-v3
```

```83f60ec8e5af``` er id på min debian container.

```my-debian-nginx-v3``` er navnet på det image jeg forsøger at oprette.

## Femte skridt

Der var et andet skridt.

Det var at køre den fulde container. Var mere besværlig end regnet med.
Hvad problemet var, var at nginx service ikke kørte.

Det bliver fikset med denne command:

```bash
docker run -dti -p 8083:80 --entrypoint "nginx" my-debian-nginx-v3 -g "daemon off;"
```


