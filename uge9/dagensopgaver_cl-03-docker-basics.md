# UV 03 Docker Basics

I dagens opgaver, vil der være en række opgaver, som I har prøvet før, samt nogle nye opgaver. Repetitionen er for at sikre at I får opgaverne ind gennem mange gentagne forsøg. Det er derfor vigtigt at I forsøger jer med det, så I kan få opgaverne godt igennem fingrene.

## Opgaver

### 01: Starte Docker container

Nu har I tidligere prøvet at bruge Nginx som webserver. I denne opgave skal I forsøge med det samme, bare med Apache webserver (ofte kaldet httpd). Prøv at starte en webserver der med denne der præsenterer følgende html fil (index.html):

```
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	<h1>Hello from Apache</h1>
</body>
</html>
```

Et typisk problem med mange eksisterende container images er at forstå at bruge de MANGE forskellige containere derude og det er derfor vigtigt at finde ud af hvordan disse containere bruges. Det repræsenterer ved at I skal finde ud af hvor ovenstående fil skal placeres.

### 02: Mount volumes

Stop nu containeren fra Opgave 01 og mount en mappe til ved et nyt run. Dermed kan du påvise live edit på denne alternative server også.

### 03: Oprydning

I har efterspurgt det og selvfølgelig leverer vi. Oprydning er en vigtigt og kan efter længere perioder fjerne store mængder plads, særligt når majoriteten og gentagen udvikling foretages.

For at rydde op kan I passende prøve kommandoerne fra følgende artikel:

<https://www.freecodecamp.org/news/how-to-remove-all-docker-images-a-docker-cleanup-guide/>

### 04: Gemme ændringer på en container

I denne opgave skal du udvikle din egen Nginx server. Dermed skal du baseret på f.eks. baseret på en Debian container selv installere software og gemme containeren som din egen Nginx container.

1. Pull en Debian linux container
2. Starte containeren og eksekver ind i containeren, hvori du skal installere Nginx web server
3. Du skal ligeledes lave din egen html fil, med lidt indhold der fortæller om hvor lækre containere er.
4. Nu skal du foretage et commit af containeren til et image (læs sætningen og brug den til at søge nødvendig viden)
5. Baseret på jeres nye image, skal i starte en container med en port, så I kan fremvise websiden

Til denne opgave må du ikke bruge **Dockerfile** og hvis du ikke ved hvad det er behøver du ikke tænke på det endnu ;-)

Nøgleord:

* apt
* commit

### 