# SQL Server Containerization Using Docker

M�let var at k�re en SQL server i en container. Jeg valgte at g�re brug af SQL server fra Microsoft.

Link til image: https://hub.docker.com/_/microsoft-mssql-server

Jeg har så valgt at g�re brug af en docker-compose.yaml fil. Denne bruges til at beskrive hvordan en container skal started op, og hvilke environmental variabler der skal v�re tilstede.

Containeren k�res med:

```bash
docker-compose up
```

```-d``` er et optionalt flag for up parametren.
