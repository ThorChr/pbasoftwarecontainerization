# CL 07: Load Balancing & Orchestration

<https://docs.google.com/presentation/d/1YVOqureCDtSWGpQgVU4ecHISbaJzBeiSqYAklaerO2w/edit?usp=sharing>

## Opgaver

### Opg 01: Basic load balancing

I denne første opgave, skal du teste load balancing og finde en måde at vurdere om trafikken flytter rammer de forskellige containere.

Det forventes at du bruger Swarm til opgaven. Det er ikke påkrævet at du laver en Stack yaml fil, eller bare services. Valget er dit.

1. Byg og kør en webapplikation i dit swarm "cluster", der har en måde at præsentere den enkelte container
2. Sørg for at skalere applikationen til en passende størrelse
3. Test at det ikke altid er den samme container der svarer

Nøgleord:

* replicas
* load balancing
* round-robin
* environment variable: HOSTNAME

### Opg 02 Afprøve Portainer

I denne opgave skal I prøve at sætte Portainer op også skal I sørge for at starte Opgave 01 op i portainer.

Det vil ydermere være en god ide at teste denne platform godt og grundigt. Det er en let vej ind I orchestration verdenen.

<https://www.portainer.io/>

<https://hub.docker.com/r/portainer/portainer-ce>

### Læsning & videoer

Læs denne guide om hvordan man med Nginx kan lave mere avanceret load balancing

* Gennemgang af Docker load balancing i Swarm <https://upcloud.com/resources/tutorials/load-balancing-docker-swarm-mode>
* Swarm og Kubernetes comparison <https://www.aquasec.com/cloud-native-academy/docker-container/docker-orchestration/>
* Kubernetes Crash course <https://www.youtube.com/watch?v=s_o8dwzRlu4>