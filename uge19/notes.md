# Docker Notes

## Mount

Ikke glad for den her del. Den skal skrives meget om. Oh well, for now så er der this stuff:

- NFS, Network File System.

Den er god at bruge i swarms. Hvis en node dør, og en ny starter op, så har den nye stadigt adgang til al den data der var fra den tidligere.

- Volumes<br/>
Det her er mere local storage. 
    - Mount
    - Bind


## User Management

- Brugere

Man kan se en liste over alle brugere i følgende fil: ```/etc/passwd```

- Passwords

Man kan se en liste over alle passwords (hashed) i følgende fil: ```/etc/shadow```

- adduser / useradd

Begge to laver en bruger. Den ene laver en fuld bruger der også har home directory og alt det. Den anden laver en mega basal bruger (basically en bruger hvis man aldrig skal logge ind). ```adduser``` er den fulde, og ```useradd``` er den basale.

- Skifte password 

Hvis man ønsker at skifte password til en bruger, så er det: ```passwd USERNAME password```

- Permissions

```r``` = read
```w``` = write
```x``` = execute

Kommandoen ```ls -lah``` giver fx et resultat der ser sådan her ud:

```bash
drwxr-xr-x 1 root root 4.0K
# Eksempel fra ubuntu dist.
-rw-r--r-- 1 neko neko  125 Feb  2 16:02 Dockerfile
```

```d``` = directory

```rwx``` = brugeren der ejer mappen kan read, write og execute.

```r-x``` = alle der er medlem af gruppen, kan read og execute.

```r-x``` = alle andre (folk der ikke er brugeren eller medlem af gruppen) kan read og execute.

```1``` = hvor mange filer der er her.

```root root``` = første root er brugeren. Andet root er gruppen.

```4.0K``` = størrelsen på filen eller mappen.