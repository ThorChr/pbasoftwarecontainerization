# Use an official Python runtime as a parent image
FROM python:3

# Define an argument for the filename
ARG FILE_NAME
ARG PORT_VALUE

# Set the working directory to /app
WORKDIR /app

# Copy the specified file into the container at /app
COPY $FILE_NAME .

# Install any needed packages specified in the specified file
RUN pip install --trusted-host pypi.python.org -r $FILE_NAME

# Install jupyter
RUN pip install jupyter

# Expose port 8888 for Jupyter notebook
EXPOSE 8888

# Set the notebook directory to a subdirectory in the mounted volume
ENV NOTEBOOK_DIR=/notebooks
RUN mkdir $NOTEBOOK_DIR
WORKDIR $NOTEBOOK_DIR

# Start Jupyter notebook when the container launches
CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--allow-root"]
