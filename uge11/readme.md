# Kort fortalt

Well dagens opgaver virker relativt simple. Jeg har startet med den her note lidt sent, så gider ikke beskrive de to første opgaver. Der er dockerfiler i de relevante mappter, som man kan kigge igennem.

# Opgave 3

Handler om at push stuff op.

Jeg endte med at gøre ```docker login``` for at logge ind til det hub ting.

Derefter taggede jeg så mit image ved at sige 
```bash
docker tag nodejs_docker_test thorchr96/nodejs_docker_test:v1
```

Herefter pushede jeg så mit image op ved at bruge 
```bash
docker push thorchr96/nodejs_docker_test:v1
```

Så kunne jeg tjekke ved https://hub.docker.com/repositories/thorchr96 for at se om det nu er der. Which it was, so yes it worked.

# Opgave 4

Starter ud med at køre følgende commando, som er hapset direkte fra deres dokumentation:

```
docker run -d -p 5000:5000 --restart always --name registry registry:2
```

Jeg taggede så et image, og pushede det op:

```bash
docker tag nodejs_docker_test localhost:5000/nodejs_docker_test:v1
docker push localhost:5000/nodejs_docker_test:v1
```

Nu prøver jeg så at slette det lokale image, og så pull det ned.

```bash
docker rmi localhost:5000/nodejs_docker_test:v1
docker pull localhost:5000/nodejs_docker_test:v1
```

Som svar får jeg:

```bash
v1: Pulling from nodejs_docker_test
Digest: sha256:a91436e82b973063670e2e973068ac3a678351f916725f71c1d0949110fd37c2
Status: Downloaded newer image for localhost:5000/nodejs_docker_test:v1
localhost:5000/nodejs_docker_test:v1
```

Såååe, guess it works???

# Opgave 5

Tanken blev: Container der automatisk starter jupyter op, efter den har installeret en masse dependencies.

Her kan man så vokume mount hen til en given mappe på ens pc.