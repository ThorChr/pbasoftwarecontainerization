# CL 04: Docker Build

I dagens opgaver skal I arbejde med at udvikle jeres egne images. Den store forskel ift. sidste gang er brugen af Dockerfile, som bredt er anset som et effektivt værktøj. Da det med effektiv brug af Dockerfile, kan gøre build af et projekt lettere.

En Dockerfile, består af en serie af instrukser, der giver mulighed for at udføre kommandoer (såsom installation af software), sætte working directories, sætte default bruger, køre opgaver og meget mere.

En velskrevet Dockerfile, sikrer at man i f.eks. et Git repository har en standard fil der kan buildes også har man et image klar til brug.

Dagens andet emne er brugen af registries. I har nu hentet flere Docker images fra Docker Hub, der er det største registry til Docker images. I dagens opgaver skal I prøve at uploade Docker images til Dockers officielle registry og ligeledes skal I prøver at oprette et test registry lokalt på jeres maskine og uploade images til dette.

I dagens opgaver, vil I opdage at I ikke må bruge officielle images såsom Nginx eller Node imagene. Docker build opgaverne, handler om at lære at bygge Dockerfiles og ikke at bruge eksisterende Docker færdige Docker images.

## Opgaver

### 01: Nginx image med Dockerfile

I undervisningsmaterialet fra sidste gang byggede I et Docker image manuelt. Hvilket som sådan fungerer fint, dog med den udfordring at vedligeholdelse af Docker imaget er ret udfordrende. Ligeledes er der ikke stor gennemsigtighed med udviklingen af Docker imaget. Derfor skal I nu udvikle det selv samme image, bar med `Dockerfile` og docker build kommandoen.

Her er trinene fra sidste gang:

1. Pull en Debian linux container
2. Starte containeren og eksekver ind i containeren, hvori du skal installere Nginx web server
3. Du skal ligeledes lave din egen html fil, med lidt indhold der fortæller om hvor lækre containere er.
4. Nu skal du foretage et commit af containeren til et image (læs sætningen og brug den til at søge nødvendig viden)
5. Baseret på jeres nye image, skal i starte en container med en port, så I kan fremvise websiden

Baseret på ovenstående skal I nu udvikler en `Dockerfile`. Når i har buildet jeres fil og bekræftet at den kan køre med `docker run`, samt testet at I kan vise jeres unikke HTML side i browseren fra containeren, kan I vurdere opgaven som færdig.

Nøgleord:

* Dockerfile
* FROM
* RUN
* WORKDIR
* EXPOSE
* CMD
* docker build
* docker run

<https://docs.docker.com/engine/reference/builder/>

### 02: Bygge en Node.js test server

Denne gang skal I bygge et Docker Image der kan køre en Node.js server. Serveren skal basere sig på et af følgende images: Debian, Alpine eller Ubuntu.

Dette Docker image skal opfylde følgende krav:

* Det skal have Node.js installeret
* Det skal kunne køre en kommando med npm fra package.json filen i git det git repository der er vedhæftet opgaven
* Når Docker imaget buildes, skal det installere Node.js dependencies med NPM kommandoen: `npm install`
* Serveren skal kunne expose vores test endpoints

Det er meget de samme nøgleord som i opgave 1 og det skal bare udføres anderledes.

Link til testkoden her: <https://github.com/ucldk/cl04-docker-nodejs>

### 03: Push Docker image til Docker Hub registry

Find ud af hvordan du pusher et Docker Image til Docker Hub, samt undersøg fordele og ulemper ved dette registry.

Sørg for at images er public og lad en medstuderende pull images.

<https://hub.docker.com>

Hint: du skal have en bruger

### 04: Opbyg jeres eget test registry

På jeres egne maskiner, skal I bygge jeres egne test registries. Disse er ikke smarte at bruge nødvendigvis, men det er en god test til at få brugt et Docker Registry

* Sørg for at der kører et Docker Registry på jeres maskiner som en container ( <https://hub.docker.com/_/registry>)
* Følg deres guide i at få den op at køre
* Sørg for at de images fra denne opgave er pushet til dette registry (der er nogle navngivningsudfordringer man skal tage højde for med images til custom registries).
* Slet Docker images fra jeres “maskine” ikke registry altså med `docker rmi [image-name]` Pull derefter jeres image på ny fra jeres lokale registry

### 05: Find nu på et Docker image

Find nu på et Docker image der kun løse en udfordring i din undervisning, dagligdag e.l. byg nu dette Docker image.

Der er altså helt frit spil, med den ene begrænsning at jeg kan finde på at bede dig præsentere dette Docker image, så husk det.

Om det er et værktøj eller en form for meme generator er sådan set ligemeget. Vi skal bare løse et eller andet i vores hverdag med dette image.

Bare husk, der findes allerede et image til dad jokes: <https://hub.docker.com/r/yesinteractive/dadjokes>