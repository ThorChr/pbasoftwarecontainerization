﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class StatusController : ControllerBase
{

    [HttpGet]
    public IActionResult Get() => Ok("All's good");

    [HttpGet("Test")]
    public IActionResult TeapotGet() => StatusCode(418, "Teapot.");
}
