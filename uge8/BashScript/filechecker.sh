#!/bin/bash

file=$1


# Check if the file exists. If not, stop script.
if [ ! -f $file ];
then
        echo "file not exist"
        exit 0
fi

# Line count.
number_of_lines=$(wc -l < $file)
echo "File has $number_of_lines lines."

# Yeet stuff out.
if [ $number_of_lines -gt 10 ];
then
    loop_count=1
    while read line
    do
        if [ ${loop_count} -le 10 ]; then
            echo $line
            loop_count=$(($loop_count + 1))
        fi
    done < $file
else
    cat $file
fi

# Finally, save new copy.
timestamp=$(date +%Y%m%d_%H%M%S)

cp $file "${file%.*}_${timestamp}.${file##*.}"
