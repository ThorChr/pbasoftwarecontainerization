
# Start

https://github.com/ucldk/cl23e-exam-project/blob/main/backend/config/database.js

Virker til at backend gør brug af en ```MySQL``` database. Good to know.

Derudover er frontend åbenbart Vue 3, backend virker til at være node.js

Der er altså forskel mellem hvordan man sætter tingene op.

Sååe frontend container, backend container, og database container? it seems like?


## docker compose vs docker swarm

eftersom det er docker swarm vi bruger, så er det vigtigt at vide lige hvilke ting man faktisk kan gøre brug af. der er nemlig forskel mellem compose og swarm. 

i swarm kan vi ikke pege hen mod en Dockerfile men skal bruge et image. I en swarm kan vi ikke sige restart policy, men det kan man godt i compose.



# Commands used

1. Klon repo ned, og naviger til backend.

```bash
git clone https://github.com/ucldk/cl23e-exam-project.git
cd cl23e-exam-project/backend
```

2. Opret dockerfile og skriv stuff.
```bash
touch Dockerfile
vim Dockerfile
```

3. Afslut VIM.
```
:wq
```

4. Naviger til frontend og opret Dockerfile.
```bash
cd ../frontend
touch Dockerfile
vim Dockerfile
```

5. Afslut VIM, og gå til root path at projekt og tjek stuff.
```bash
:wq
cd ..
pwd
ls ./backend
ls ./frontend
```

6. Opret ```docker-compose.yaml``` i root dir.
```bash
touch docker-compose.yaml
code docker-compose.yaml
```

(Ved godt at det ikke er nødvendigt at starte med touch før vim, men det er en bekvemmeligheds ting for mig at gøre det, sååe ye)






# Litteratur liste

[Dockerizing Vue.js App With NodeJS Backend](https://medium.com/bb-tutorials-and-thoughts/dockerizing-vue-app-with-nodejs-backend-33645f0f50ec)

dog vedr. ^ idk helt. det er et maybe. den starter frontend og backend op i samme container, seems weird. men maybe


# Docker commands

frontend
```
docker build -t exam_frontend .
```

backend
```
docker build -t exam_backend .
```

# Ports

backend: 3000
frontend: 8080
maria database: 3306 


# problems

fejl med paths i vue filerne.

```
assets 
vs
/assets
```

fejl når man forsøger at køre dev, mangler param til at expose stuff.

```
"vite --host 0.0.0.0"
```

hvis man laver en database til et volume til at starte med.. så selv hvis man genstarter den database-server i fremtiden, laver den ikke nogle nye databaser, selv hvis de er specified i environment variables... for at fikse, skal man yeet den gamle volume... sååe det blev gjort.

derudover, er der også et issue i koden hvor at den kan håndtere en path til en api når man er i development mode, men den eksisterer ikke for production. der får man en fejl, fordi den ikke er specified. så det skal fikses ved at ændre i koden og tilføje det.

```js
const config = {
  development: {
    api: 'http://localhost:3000',
  },
  production: {
    api: 'http://localhost:3000' // this is new.
  }
};
```

Når man laver et nyt product bliver dens billede / thumbnail til 

```/assets/ladida.png```

dette eksisterer ikke.

Min fiks: Smid et billede ind i den mappe, og kald det ```ladida.png```
PS. ikke ind i images mappen, men i assets mappen....